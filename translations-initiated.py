import pandas as pd
import numpy as np
import glob
import re
import sys

if hasattr(sys, 'ps1'):
    import copy
    debug = True
else:
    debug = False

trnsltd = pd.read_csv('/home/mlncn/Nextcloud/agaric-nextcloud/agaric/clients/FindItCambridge/analytics/translated-pages-20200614-20210615.csv', thousands=',')

if debug:
   imported = copy.deepcopy(trnsltd)

trnsltd.drop(columns = ['Avg. Time on Page', 'Entrances', 'Bounce Rate', '% Exit', 'Page Value'], inplace=True)
cols = trnsltd.columns.drop('Page')
pattern = '.+\?language=([a-z][a-z]).*'
language = trnsltd.Page.str.extract(pattern, expand=True)
trnsltd['Language'] = language
"trnsltd.drop(columns = [], inplace=True)"

languages = trnsltd.groupby('Language').agg({'Pageviews': 'sum', 'Unique Pageviews': 'sum'})
sorted_languages = languages.sort_values(by='Pageviews', ascending=False)

if not debug:
    sorted_languages.to_csv('translations-initiated-by-language.csv', index=False)
else:
    print("We do not write to CSV nor update the latest recorded setting when run interactively in the python shell.")
