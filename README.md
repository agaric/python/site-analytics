Pandas scripts for analyzing web analytics CSV exports.  Currently one script: Roll up languages used (from query string) with a count of page views, ordered by most viewed.

Get all the languages that people have used on a website (that uses Google Translate, with the query string) as extracted from Google Analytics.  Given that the query string goes away on subsequent page views, this can be considered the number of translation sessions initiated.

NOTE: Delete junk that Google adds to the top and bottom of the CSV, this script does not account for that.

## Installation

### Prerequisites

#### Install venv and pip- and python, too!

```
sudo apt install python3-venv python3-pip
```

This will also install python3 if it isn't already.

We don't use venv in these instructions but you can if you want to sort of sandbox this project.

Pip is needed.

#### Make Python 3 the default

If `python --version` does not give you some variant of Python 3.x.y:

```
sudo su
update-alternatives --install /usr/bin/python python /usr/bin/python3 1
exit
```

If you don't do the above, substitute `python3` for `python` in all following commands.

### Install

```
mkdir -p ~/Projects/agaric/python
cd ~/Projects/agaric/python
git clone git@gitlab.com:agaric/python/site-analytics.git ~/Projects/agaric/python
cd ~/Projects/agaric/python/site-analytics
python -m pip install --user -r requirements.txt
```

## Usage

```bash
python translations-initiated.py
```

# Learning from this script and continuing development

Rather than having to type out each line of this script to manipulate it at different points, you can also run this script in the interactive shell and play with it.

Type `python` to get the interactive Python shell in this directory, then:

```python
exec(open('transactions-rollup.py').read())
```

And now you can interact with the resulting timelog DataFrame.
